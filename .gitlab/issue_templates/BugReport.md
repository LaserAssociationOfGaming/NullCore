/label ~"type::Unconfirmed"
/label ~"type::Bug"

## Information
 - Null Core Version: [e.g. v1.1.0 Bata 1]
 - Minecraft Version: [e.g. 1.19.2]
 - Section of the Framework: [e.g. Configuration Package]

## Unexpected Behavior
A clear and concise description of what the bug is.

## Expected Behavior
A clear and concise description of what you expected to happen.

## Steps to Reproduce
Steps to reproduce the behavior:
1. Import the ----- package from Null Core
2. Use the ----- method (also how did you use the method?)
3. Receive the error -----

## Additional Context
If applicable, add extra context or screenshots to help explain your problem.
