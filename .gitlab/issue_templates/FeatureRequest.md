/label ~"type::Unconfirmed"
/label ~"type::Feature"

## Describe the feature you would like added
A clear and concise description of what you want to happen.

## Is your feature request related to a problem you encounter?
A clear and concise description of what the problem is. Ex. I'm always frustrated when [...]

## Theoretical approaches or alternative solutions?
A clear and concise description of any alternative solutions or features you've considered.

## Additional context
Add any other context or screenshots about the feature request here.
